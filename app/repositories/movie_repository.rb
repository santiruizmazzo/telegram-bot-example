class MovieRepository
  API_KEY = ENV['OMDB_API_KEY']
  API_URL = 'https://www.omdbapi.com/'.freeze
  MOVIE_NOT_FOUND_ERROR_MESSAGE = 'Movie not found!'.freeze
  NO_AWARDS_MESSAGE = 'N/A'.freeze

  def find_by_title(movie_title)
    response = Faraday.get(API_URL, { t: movie_title, apikey: API_KEY })
    response_json = JSON.parse(response.body)
    raise MovieNotFound if response_json['Response'] == 'False' && response_json['Error'] == MOVIE_NOT_FOUND_ERROR_MESSAGE

    awards = response_json['Awards'] == NO_AWARDS_MESSAGE ? "It hasn't won any awards" : response_json['Awards']

    Movie.new(movie_title, awards)
  end
end

class MovieNotFound < RuntimeError; end
