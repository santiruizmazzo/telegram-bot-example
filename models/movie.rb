class Movie
  attr_reader :title, :awards

  def initialize(title, awards)
    @title = title
    @awards = awards
  end
end
