require 'spec_helper'
require 'web_mock'
require_relative '../app/repositories/movie_repository'

def stub_omdb_test(movie_title, api_omdb_response_body)
  stub_request(:get, "https://www.omdbapi.com/?apikey&t=#{movie_title}").with(
    headers: {
      'Accept' => '*/*',
      'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
      'User-Agent' => 'Faraday v2.7.4'
    }
  ).to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

describe MovieRepository do
  describe 'find_by_title' do
    it 'should get movie whose title is Coco' do
      movie_title = 'Coco'
      stub_omdb_test(movie_title, { 'Title': movie_title })
      movie = described_class.new.find_by_title(movie_title)
      expect(movie.title).to eq movie_title
    end

    it 'should raise error when movie cannot be found' do
      stub_omdb_test('Fulanito', { 'Response': 'False', 'Error': 'Movie not found!' })

      expect do
        described_class.new.find_by_title('Fulanito')
      end.to raise_error(MovieNotFound)
    end

    it "should get movie that hasn't won any awards" do
      stub_omdb_test('Terminator', { 'Title': 'Terminator', 'Awards': 'N/A' })
      movie = described_class.new.find_by_title('Terminator')
      expect(movie.awards).to eq "It hasn't won any awards"
    end
  end
end
