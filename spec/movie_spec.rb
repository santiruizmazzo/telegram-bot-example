require 'spec_helper'
require_relative '../models/movie'

describe Movie do
  subject(:movie) { described_class.new('Cars', 'Nominated for 2 Oscars. 28 wins & 34 nominations total') }

  describe 'model' do
    it { is_expected.to respond_to(:title) }
    it { is_expected.to respond_to(:awards) }
  end
end
