Feature:
  As a cinephile
  I want to ask for the awards a certain movie has won

  Scenario: Movie does not exist
    Given the movie "Fulanito" does not exist
    When I ask for its awards
    Then the bot says it does not exist
  
  Scenario: Movie has awards
    Given the movie "Titanic" has awards
    When I ask for its awards
    Then the bot shows that it has won "11 Oscars"

  Scenario: Movie does not have any awards
    Given the movie "Terminator" does not have any awards
    When I ask for its awards
    Then the bot says that it does not have any awards
  
  Scenario: Ask for the awards of multiple movies
    Given the movie "Titanic" has awards
    And the movie "Parasite" has awards
    When I ask for their awards
    Then the bot shows that it "Titanic" has won 11 Oscars and "Parasite" has won 4 Oscars
